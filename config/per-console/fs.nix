{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    ntfs3g hfsprogs exfat exfat-utils
    gptfdisk
    gocryptfs
  ];
  
  programs.fuse.userAllowOther = true;
}
