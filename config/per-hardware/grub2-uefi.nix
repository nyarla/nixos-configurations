{ config, pkgs, ... }:
{
  boot.loader.grub = {
    enable       = true;
    version      = 2;
    efiSupport   = true;
    device       = "nodev";
    font         = "${pkgs.terminus_font}/share/fonts/terminus/ter-x32n.pcf.gz";
    fontSize     = 32;
    useOSProber  = true;
  };

  boot.loader.efi.canTouchEfiVariables = true;
}
