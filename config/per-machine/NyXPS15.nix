{ config, pkgs, ... }:
{
  i18n.defaultLocale = "ja_JP.UTF-8";
  i18n.supportedLocales = [
    "ja_JP.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
  ];

  networking.hostId = "71ca914d";
  networking.hostName = "NyXPS15";

  imports = [
    ../per-hardware/bluetooth.nix
    ../per-hardware/console.nix
    ../per-hardware/grub2-uefi.nix
    ../per-hardware/intel-nvidia.nix
    ../per-hardware/keyboard-us.nix
    ../per-hardware/pulseaudio.nix
    ../per-hardware/tcp-bbr.nix
    ../per-hardware/thunderbolt.nix
    ../per-hardware/XPS-9560-JP.nix
    
    ../per-service/avahi.nix
    ../per-service/firewall.nix
    ../per-service/printer.nix
  ];
}
