" Core
" ====

" VIM
" ---
set nocompatible

" File Types
" ----------
syntax on
filetype on
filetype plugin indent on

" Editor
" ------
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

set backspace=indent,eol,start

set clipboard=unnamed " set clipboard&

" Interface
" ---------
set number
set laststatus=2
set showtabline=2
set ambiwidth=double

colorscheme smyck

set completeopt-=preview

" Backup
" ------
set directory=~/.vim.d/swap
set backupdir=~/.vim.d/backup

" TTY
" ---
set mouse=a

" Tabline
" -------
function! s:tpl_label(n)
  let label = gettabvar(a:n, 'title')

  if label ==# ''
    let buffers = tabpagebuflist(a:n)
    let count   = len(buffers)

    if count > 1
      let label = count . ' buffers'
    else
      let label = getbufvar(buffers[0], '&ft')
    endif
  endif

  if label ==# ''
    let label = 'new' 
  endif

  let prefix = ''
  if a:n is tabpagenr()
    let prefix = '%#TabLineSel#%#TabLineMarker#❯ %#TabLineSel#'
  else
    let prefix = '%#TabLineNum#' . a:n . '%#TabLine# '
  endif

  return '%' . a:n . 'T' . prefix . label . '%T%#TabLineFill#'
endfunction

function! s:tpl_call(cmd)
  return substitute(system(a:cmd), '\n\+$', '', '')
endfunction

function! s:tpl_info(tag, icon, data)
  return '%#TabLine' . a:tag . '#' . a:icon . '%#TabLineFill# ' . a:data . ' '
endfunction

function! MakeTabLine()
  let pages   = map(range(1, tabpagenr('$')), 's:tpl_label(v:val)')
  let sep     = ' '
  let label   = join(pages, sep) . sep . '%#TabLineFill#'

  let machine = s:tpl_info('Machine', '', s:tpl_call('hostname')) 
  let user    = s:tpl_info('Account', '', $USER)

  let datetime = split(s:tpl_call('date "+%Y-%m-%d %H:%M:%S"'), ' ')
  let date     = s:tpl_info('Date',   '', datetime[0])
  let time     = s:tpl_info('Time',   '', datetime[1])

  return label . '%=' . date . time . user . machine
endfunction

function! UpdateTabLine(timer)
  set tabline=%!MakeTabLine()
endfunction

let s:tabline_timer = timer_start(1000, function('UpdateTabLine'), { 'repeat': -1 })
au User ExitPre * call timer_stop(tabline_timer)
set tabline=%!MakeTabLine()

hi TabLine ctermfg=15 ctermbg=none cterm=none
hi TabLineFill ctermfg=15 ctermbg=none cterm=none
hi TabLineCell ctermfg=15 ctermbg=none cterm=none
hi TabLineInfo ctermfg=15 ctermbg=none cterm=none

hi TabLineMarker ctermfg=12
hi TabLineNum ctermfg=2
hi TabLineMachine ctermfg=10
hi TabLineAccount ctermfg=12
hi TabLineTime ctermfg=14
hi TabLineDate ctermfg=11

" KeyMaps
" -------

" workaround (?)
map j <M-j>
map k <M-k>

" KeyMap for tmux-like (<prefix> = <C-t>)
" --------------------

" <prefix> + c -> :tabnew
nnoremap <C-t>c :tabnew<CR>
inoremap <C-t>c <Esc>:tabnew<CR>
tnoremap <C-t>c <C-\><C-n><Esc>:tabnew<CR>

"<prefix> + q -> <Esc>
inoremap <C-t>q <Esc>
tnoremap <C-t>q <C-\><C-n><Esc>

" <prefix> + x -> close buffer
nnoremap <C-t>x :q<CR>
inoremap <C-t>x <Esc>:q<CR>
tnoremap <C-t>x <C-\><C-n><Esc>:q<CR>

" <prefix> + X -> kill bufffer
nnoremap <C-t>X :q!<CR>
inoremap <C-t>X <Esc>:q!<CR>
tnoremap <C-t>X <C-\><C-n><Esc>:q!<CR>

" <prefix> + {n} -> move to {n}th tab
nnoremap <C-t>1 1gt
inoremap <C-t>1 <Esc>1gt
tnoremap <C-t>1 <C-\><C-n><ESC>1gt

nnoremap <C-t>2 2gt
inoremap <C-t>2 <Esc>2gt
tnoremap <C-t>2 <C-\><C-n><ESC>2gt

nnoremap <C-t>3 3gt
inoremap <C-t>3 <Esc>3gt
tnoremap <C-t>3 <C-\><C-n><ESC>3gt

nnoremap <C-t>4 4gt
inoremap <C-t>4 <Esc>4gt
tnoremap <C-t>4 <C-\><C-n><ESC>4gt

nnoremap <C-t>5 5gt
inoremap <C-t>5 <Esc>5gt
tnoremap <C-t>5 <C-\><C-n><ESC>5gt

nnoremap <C-t>6 6gt
inoremap <C-t>6 <Esc>6gt
tnoremap <C-t>6 <C-\><C-n><ESC>6gt

nnoremap <C-t>7 7gt
inoremap <C-t>7 <Esc>7gt
tnoremap <C-t>7 <C-\><C-n><ESC>7gt

nnoremap <C-t>8 8gt
inoremap <C-t>8 <Esc>8gt
tnoremap <C-t>8 <C-\><C-n><ESC>8gt

nnoremap <C-t>9 9gt
inoremap <C-t>9 <Esc>9gt
tnoremap <C-t>9 <C-\><C-n><ESC>9gt

" <prefix> + | -> :vsp
nnoremap <C-t>\| :vsp<CR>
inoremap <C-t>\| <Esc>:vsp<CR>
tnoremap <C-t>\| <C-\><C-n><Esc>:vsp<CR>

" <prefix> + - -> :sp
nnoremap <C-t>- :sp<CR>
inoremap <C-t>- <Esc>:sp<CR>
tnoremap <C-t>- <C-\><C-n><Esc>:sp<CR>

" <prefix> + {arrow} -> move to buffer
nnoremap <S-Left> <C-w><Left>
inoremap <S-Left> <Esc><C-w><Left>
tnoremap <S-Left> <C-\><C-n><Esc><C-w><Left>

nnoremap <S-Right> <C-w><Right>
inoremap <S-Right> <Esc><C-w><Right>
tnoremap <S-Right> <C-\><C-n><Esc><C-w><Right>

nnoremap <S-Up> <C-w><Up>
inoremap <S-Up> <Esc><C-w><Up>
tnoremap <S-Up> <C-\><C-n><Esc><C-w><Up>

nnoremap <S-Down> <C-w><Down>
inoremap <S-Down> <Esc><C-w><Down>
tnoremap <S-Down> <C-\><C-n><Esc><C-w><Down>


" Plugins
" =======

" Pre-configure for plugins
" -------------------------

" Interface
let g:airline_powerline_fonts = 1
let g:airline_theme = "powerlineish"
let g:airline_left_sep = ""
let g:airline_left_alt_sep = ""
let g:airline_right_sep = ""
let g:airline_right_alt_sep = ""

let g:lsp_signs_enabled = 1
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_signs_error = {'text': '!!'}
let g:lsp_signs_warning = {'text': '!?'}
let g:lsp_signs_hint = {'text': '??'}

let g:asyncomplete_auto_popup = 0
let g:asyncomplete_remove_duplicates = 1
let g:asyncomplete_smart_completion = 1


" File Types
let g:markdown_fenced_languages = [ "json", "yaml", "idl" ]

" Enabling Plugins
" ----------------
call plug#begin('$HOME/.config/nvim/plugged')

" Interface
Plug 'ajh17/VimCompletesMe'
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Editor
Plug 'Chiel92/vim-autoformat'
Plug 'editorconfig/editorconfig-vim'

" Auto Complete
Plug 'prabirshrestha/asyncomplete.vim'                    " auto-complete plugin
Plug 'prabirshrestha/async.vim'                           " for vim-lsp dependence
Plug 'prabirshrestha/vim-lsp'                             " for Language Protocol Server dependences
Plug 'prabirshrestha/asyncomplete-lsp.vim'                " for Language protocol server
Plug 'runoshun/tscompletejob'                             " for typescript dependences

Plug 'keremc/asyncomplete-racer.vim'                      " for rust
Plug 'prabirshrestha/asyncomplete-tscompletejob.vim'      " for typescript

Plug 'yami-beta/asyncomplete-omni.vim'                    " for fallback

" File Types
Plug 'freitass/todo.txt-vim'                              " for todo.txt

Plug 'fatih/vim-go'                                       " for golang
Plug 'rust-lang/rust.vim'                                 " for rust
Plug 'racer-rust/vim-racer',      { 'for': 'rust' }       " for rust

Plug 'keith/swift.vim'                                    " for Apple's Swift
Plug 'zah/nim.vim'                                        " for nim-lang

Plug 'isRuslan/vim-es6',          { 'for': 'javascript' } " for JavaScript
Plug 'MaxMEllon/vim-jsx-pretty',  { 'for': 'javascript' } " for Javascript With JSX
Plug 'posva/vim-vue'                                      " for Vue.js 

Plug 'leafgarland/typescript-vim'                         " for typescript
Plug 'ElmCast/elm-vim'                                    " for elm-lang
Plug 'purescript-contrib/purescript-vim'                  " for purescript

Plug 'alunny/pegjs-vim'                                   " for peg.js

Plug 'vim-perl/vim-perl',         { 'for': 'perl' }       " for Perl

Plug 'derekwyatt/vim-scala'                               " for Scala

Plug 'jparise/vim-graphql'                                " for GraphQL
Plug 'uarun/vim-protobuf'                                 " for Protobuf

Plug 'othree/html5.vim',          { 'for': 'html' }       " for HTML5 
Plug 'hail2u/vim-css3-syntax',    { 'for': 'css'  }       " for CSS

Plug 'rhysd/vim-gfm-syntax',      { 'for': 'markdown' }   " for GitHub-flavoured Markdown

Plug 'ekalinin/Dockerfile.vim'                            " for Dockerfile

Plug 'cespare/vim-toml'                                   " for TOML
Plug 'LnL7/vim-nix'                                       " for nix

call plug#end()

" Plugin Configurations
" ---------------------

if exists('$HOME/.config/plugged/asynccomplete-racer.vim')
  au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#racer#get_source_options({
    \ 'name': 'racer',
    \ 'whitelist': ['rust'],
    \ 'completer': function('asyncomplete#sources#racer#completor'),
    \ 'config': {
    \   'racer_path': 'racer',
    \ },
    \ }))
endif

if exists('$HOME/.config/plugged/asyncomplete-tscompletejob.vim')
  au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#tscompletejob#get_source_options({
    \ 'name': 'tscompletejob',
    \ 'whitelist': ['typescript'],
    \ 'completor': function('asyncomplete#sources#tscompletejob#completor'),
    \ }))
endif

if exists('$HOME/.config/plugged/asyncomplete-omni.vim')
  au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
      \ 'name': 'omni',
      \ 'whitelist': ['*'],
      \ 'blacklist': ['vim', 'html', 'elm'],
      \ 'completor': function('asyncomplete#sources#omni#completor')
      \  }))
endif

" Language protocol server
if executable('bash-language-server')
  au User lsp_setup call lsp#register_server({
      \ 'name': 'bash-language-server',
      \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
      \ 'whitelist': ['sh', 'bash'],
      \ })
endif

if executable('css-languageserver')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'css-languageserver',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'css-languageserver --stdio']},
    \ 'whitelist': ['css', 'less', 'sass'],
    \ })
endif

if executable('docker-langserver')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'docker-langserver',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'docker-langserver --stdio']},
    \ 'whitelist': ['dockerfile'],
    \ })
endif

if executable('bingo')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'golang-language-server',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bingo -mode stdio']},
    \ 'whitelist': ['go'],
    \ })
endif

if executable('vscode-json-languageserver')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'purescript-language-server',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'vscode-json-languageserver --stdio']},
    \ 'whitelist': ['json'],
    \ })
endif


if executable('purescript-language-server')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'purescript-language-server',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'purescript-language-server --stdio']},
    \ 'whitelist': ['purescript'],
    \ })
endif

if executable('typescript-language-server')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'typescript-language-server',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'typescript-language-server --stdio']},
    \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
    \ 'whitelist': ['javascript', 'javascript.jsx'],
    \ })
endif

" File Types
au BufWrite *.ts :Autoformat
au BufWrite *.js :Autoformat
au BufWrite *.json :Autoformat

au BufRead,BufNewFile *.purs set filetype=purescript      " for purescript

augroup ColorSchemeTodoTxt                                " for todo.txt
  au!
  autocmd ColorScheme * highlight TodoProject ctermfg=blue guifg=#9cd9f0
  autocmd ColorScheme * highlight TodoContext ctermfg=green guifg=#cdee69
  autocmd ColorScheme * highlight TodoDate ctermfg=yellow guifg=#ffe377
augroup END

augroup Terminal
  au!
  au TermOpen * startinsert
augroup END

" KeyMaps
imap <c-space> <Plug>(asyncomplete_force_refresh)
