{ config, pkgs, ... }:
let
  apps = with pkgs; [
    pavucontrol lxappearance-gtk3 arandr
    light hsetroot wmctrl xdotool graphicsmagick xtitle
    xorg.xsetroot xorg.xwininfo xorg.xdpyinfo xorg.xrdb
  ];
in {
  environment.systemPackages = apps;
  services.dbus.packages = apps;
}
