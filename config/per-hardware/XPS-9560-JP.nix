{ config, pkgs, ... }:
{
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.initrd.luks.devices = [
    { name   = "container";
      device = "/dev/disk/by-uuid/733af657-a327-46cc-ad7f-ca81e3d5aed6";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  boot.kernelParams  = [
    "acpi_rev_override=5"
    "pcie_port_pm=off"
    "snd_hda_intel.power_save=0"
    "snd_hda_intel.power_save_controller=0"
    "usb_storage.quirks=152d:0578:u"
  ];

  boot.cleanTmpDir = true;
  boot.tmpOnTmpfs  = false;

  fileSystems."/".options = [
    "noatime" "nodiratime" "discard"
  ];

  fileSystems."/boot".options = [
    "noatime" "nodiratime" "discard"
  ];

  fileSystems."/nix".options = [
    "noatime" "nodiratime" "discard"
  ];

  fileSystems."/home".options = [
    "noatime" "nodiratime" "discard"
  ];

  powerManagement.enable = true;
  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils}/bin/chgrp video /sys/class/backlight/%k/brightness"
    ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils}/bin/chmod g+w /sys/class/backlight/%k/brightness"
  '';
}
