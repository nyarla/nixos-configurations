{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    awscli google-cloud-sdk
    nixops
  ]; 
}
